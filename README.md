# Juneau

Juneau is a Discord bot custom tailored to the needs of a private programming assistance Discord. The main focus of Juneau is ease-of-use
and assistance.

The Discord server is created for my college classes for anyone who is enrolled in them to take part in assisting other students if they are
struggling with something in their class.

# Current Features:
* Stack Overflow statistics
* Tag System
* Color Decoding
* Permissions System
* Live-Fetching Changelog Command

# Planned:
* Class Registration
* Support Tickets?
* More general assistance commands
