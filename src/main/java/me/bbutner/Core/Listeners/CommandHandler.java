package me.bbutner.Core.Listeners;

import com.vdurmont.emoji.EmojiManager;
import me.bbutner.Core.Command;
import me.bbutner.Core.Juneau;
import me.bbutner.Utils.Logging;
import me.bbutner.Utils.Permissions;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.ArrayList;

/**
 * Handles commands being passed to Juneau
 * @author bbutner
 * @version 1.0
 */
public class CommandHandler {
    private static ArrayList<Command> commands = new ArrayList<>();

    /**
     * Processes the command
     * @param event Used for the message and the author
     */
    public static void processCommand(MessageReceivedEvent event) {
        String commandName = event.getMessage().getContent().split(" ")[0].replace(Juneau.CMD_PREFIX, "");
        String[] args = event.getMessage().getContent().replace(Juneau.CMD_PREFIX + commandName, "").split(" ");

        if (isCommand(commandName.toLowerCase()) && Permissions.hasPermissionForCommand(getCommand(commandName.toLowerCase()), event.getAuthor())) {
            getCommand(commandName.toLowerCase()).runCommand(event, args);
        } else {
           
            event.getMessage().addReaction(EmojiManager.getForAlias(isCommand(commandName.toLowerCase()) ? "x" : "question"));
        }
    }

    /**
     * Gets the command from name
     * @param name The name of the command
     * @return The command
     */
    public static Command getCommand(String name) {
        return commands.stream().filter(cmd -> name.toLowerCase().equals(cmd.getName())).findAny().orElse(null);
    }

    /**
     * Checks if the command exists
     * @param name The name of the command
     * @return If the command exists
     */
    public static Boolean isCommand(String name) {
        return commands.stream().anyMatch(cmd -> name.toLowerCase().equals(cmd.getName()));
    }

    static void registerCommands() {
        commands.add(new Command("help", "List all commands, what they do, and how to use them.", "help", 1));
        commands.add(new Command("info", "Shows general information and statistics for Juneau.", "info", 1));
        commands.add(new Command("clear", "Clears all messages that are either bot messages or a command.", "clear", 3));
        commands.add(new Command("changelog", "Shows the recent changes submitted to Juneau's GitHub page.", "changelog", 1));
        commands.add(new Command("tag", "Tags system", "tag", 1));
        commands.add(new Command("color", "Gives the Hex, RGB, RGB 1.0, and Integer information of a color.", "color [color]", 1));
        commands.add(new Command("log", "Shows the latest 15 lines of Juneau's log.", "log", 4));
        commands.add(new Command("userinfo", "Gets information about yourself or another user.", "userinfo [user]", 1));
        commands.add(new Command("role", "Gets information about yourself or another user.", "role [user]", 1));

       

        Logging.infoBot("Commands registered.");
    }

    /**
     * Gets the list of commands
     * @return the commands
     */
    public static ArrayList<Command> getCommands() {
        return commands;
    }

}
