package me.bbutner.Core.Listeners;

import me.bbutner.Core.Juneau;
import me.bbutner.Core.Reflections;
import me.bbutner.Utils.Database;
import me.bbutner.Utils.Logging;
import me.bbutner.Utils.Permissions;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;

/**
 * Initializes systems for Juneau after he has logged in
 * @author bbutner
 * @version 1.0
 */
public class ReadyHandler {

    /**
     * Method called after Juneau has logged in
     * @param event The event
     */
    @EventSubscriber
    public void onReadyEvent(ReadyEvent event) {
        Logging.infoBot("Logged in.");
        Juneau.getClient().changePlayingText(Juneau.CMD_PREFIX + "help for commands!");
        Reflections.initialize();
        CommandHandler.registerCommands();
        Permissions.initPermissions();
        Database.connectDatabase();
        Juneau.getDispatcher().registerListener(new MessageHandler());
    }

}
