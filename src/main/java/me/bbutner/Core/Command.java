package me.bbutner.Core;

import me.bbutner.Utils.Logging;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Class for creating Command objects
 * @author bbutner
 * @version 1.0
 */
public class Command {
    private String cmdName;
    private String cmdDescription;
    private String cmdHelp;
    private int cmdPerm;

    /**
     * Creates the command object
     * @param name The name of the command
     * @param desc The description of the command
     * @param help Command usage
     * @param perm Command permission level
     */
    public Command(String name, String desc, String help, int perm) {
        this.cmdName = name;
        this.cmdDescription = desc;
        this.cmdHelp = Juneau.CMD_PREFIX + help;
        this.cmdPerm = perm;
    }

    /**
     * Run the command
     * On error, prints out the first 10 lines of the error to the channel it was run in
     * @param event The event thrown by the dispatcher
     * @param args The command arguments
     */
    public void runCommand(MessageReceivedEvent event, String[] args) {
        Logging.infoCommand(event.getAuthor(), this);
        Method method = me.bbutner.Core.Reflections.getCommandMethod(this.getName());
        method.setAccessible(true);
        try {
            method.invoke(null, event, args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            String errorLines = "";

            for (int ctr = 0; ctr < 10; ctr++) {
                if (ctr < cause.getStackTrace().length) {
                    errorLines += cause.getStackTrace()[ctr] + "\r\n";
                }
            }

            EmbedBuilder errorEmbed = new EmbedBuilder().withTitle("Error!")
                    .withDescription(String.format("The command **%s** has encountered an error.", this.getName()))
                    .withColor(new Color(255, 71, 71))
                    .withThumbnail(Juneau.getClient().getOurUser().getAvatarURL())
                    .appendField("Log:", String.format("```java\r\n%s```", String.format("Showing first 10 lines of error...\r\n\r\nCaused By: %s\r\n%s", cause, errorLines)), false);

            event.getChannel().sendMessage(errorEmbed.build());
        }
    }

    /**
     * Returns the name of the command
     * @return the command name
     */
    public String getName() {
        return this.cmdName;
    }

    /**
     * Returns the description of the command
     * @return the description
     */
    public String getDescription() {
        return this.cmdDescription;
    }

    /**
     * Returns the command usage
     * @return the usage
     */
    public String getHelp() {
        return this.cmdHelp;
    }

    /**
     * Returns the permission level required to run the command
     * @return the permission level
     */
    public int getPerm() {
        return this.cmdPerm;
    }
}
