package me.bbutner.Core;

import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Initializes and scans the Commands package for all classes
 * @author bbutner
 * @version 1.0
 */
public class Reflections {
   
    private static List<ClassLoader> classLoadersList = new LinkedList<>();
    private static Set<Class<?>> classes;

    /**
     * Scans the Commands package, then loads all of them into the classes Set
     */
    public static void initialize() {
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
        org.reflections.Reflections reflections = new org.reflections.Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("me.bbutner.Commands"))));
        classes = reflections.getSubTypesOf(Object.class);
    }

    /**
     * Gets the handleCommand method for the corresponding command
     * @param name The name of the command
     * @return the command
     */
    static Method getCommandMethod(String name) {
        Method method = null;
        for (Class clazz: classes) {
            if (clazz.getName().replace("me.bbutner.Commands.", "").toLowerCase().equals(name)) {
                method = Arrays.stream(clazz.getDeclaredMethods()).filter(method1 -> "handleCommand".equals(method1.getName())).findAny().orElse(null);
            }
        }
        return method;
    }

}
