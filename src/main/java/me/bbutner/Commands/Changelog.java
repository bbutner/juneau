package me.bbutner.Commands;

import me.bbutner.Core.Juneau;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

import java.io.IOException;

/**
 * Class to get the 10 recent commits
 * @author bbutner
 * @version 1.0
 */
public class Changelog {

    /**
     * Handles the changelog command
     * @param event Used for the channel
     * @param args Arguments supplied by the user
     */
    public static void handleCommand(MessageReceivedEvent event, String[] args) {
        try {
            Document html = Jsoup.connect("https://github.com/bbutner/Juneau/commits/master").get();
            Elements commits = html.getElementsByClass("commit commits-list-item table-list-item js-navigation-item js-details-container Details js-socket-channel js-updatable-content");
            int ctr = 0;
            EmbedBuilder changelogEmbed = new EmbedBuilder().withAuthorIcon("https://avatars3.githubusercontent.com/u/21320615?v=3&s=72")
                    .withAuthorName("GitHub")
                    .withAuthorUrl("http://github.com/bbutner/Juneau")
                    .withDescription("Showing last 10 changes in GitHub.")
                    .withColor(Juneau.EMBED_COLOR);

            while (ctr < 10 && ctr < commits.size()) {
                changelogEmbed.appendField(commits.get(ctr).child(1).child(0).child(0).html(),
                        String.format("Link: [%s](http://github.com%s)", commits.get(ctr).child(2).child(0).child(1).html(),
                                commits.get(ctr).child(2).child(0).child(1).attr("href")), false);
                ctr++;
            }

            event.getChannel().sendMessage(changelogEmbed.build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
