package me.bbutner.Commands;

import me.bbutner.Core.Listeners.MessageHandler;
import me.bbutner.Utils.TimedDeletion;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

import java.util.ArrayList;

/**
 * Class that clears Juneau's messages
 * @author bbutner
 * @version 1.0
 */
public class Clear {

    /**
     * Handles the clear command
     * @param event Used for the channel
     * @param args Not used
     */
    public static void handleCommand(MessageReceivedEvent event, String[] args) {
        clearMessages(event.getChannel());
    }

    /**
     * Clears Juneau's messages and ran commands from the channel
     * @param channel The channel for the messages to be deleted from
     */
    public static void clearMessages(IChannel channel) {
        int deleteCount = MessageHandler.getCommandCount();

        if (deleteCount > 99) {
            ArrayList<IMessage> commandMessages = new ArrayList<>();

            int ctr = 0;
            while (ctr <= 99) {
                commandMessages.add(MessageHandler.getBotMessages().get(ctr));
                ctr++;
            }

            channel.bulkDelete(commandMessages);

            TimedDeletion.delayMessageDeletion(channel.sendMessage("Clear has removed **100** messages."));
        } else {
            channel.bulkDelete(MessageHandler.getBotMessages());

            TimedDeletion.delayMessageDeletion(channel.sendMessage(String.format("Clear has removed **%s** messages.", deleteCount)));
        }
    }

}
