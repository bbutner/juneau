package me.bbutner.Utils;

import me.bbutner.Core.Juneau;

import java.sql.*;
import java.util.Properties;

/**
 * Class that interacts with the database
 * @author bbutner
 * @version 1.0
 */
public class Database {

    private static Connection connection;

    /**
     * Submit query that we don't want the result of
     * @param query The SQL String
     */
    static void submitQuery(String query) {
        if (!connectionAlive()) {
            connectDatabase();
        }

        try {
            Statement statement = connection.createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Submit a query to the database that we want the result from
     * @param query The SQL string
     * @return the ResultSet from the query
     */
    static ResultSet submitReturnQuery(String query) {
        if (!connectionAlive()) {
            connectDatabase();
        }

        try {
            Statement statement = connection.createStatement();
            statement.execute(query);

            ResultSet result = statement.getResultSet();

            try {
                result.next();
            } catch (NullPointerException ex) {
                return null;
            }

            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Check if the connection is still alive
     * @return if the database connection is still alive
     */
    private static Boolean connectionAlive() {
        try {
            return connection.isValid(1000);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Connect to the database
     * @return the connection
     */
    public static Connection connectDatabase() {
        try {
           
            Properties properties = new Properties();
            properties.setProperty("user", "juneau_user");
            properties.setProperty("password", Juneau.getPassword());
            properties.setProperty("useSSL", "false");

            connection = DriverManager.getConnection("jdbc:mysql://" + Juneau.getHostname() + ":3306/juneau_db", properties);

            Logging.infoBot("Connected to MySQL.");

            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
